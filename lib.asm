section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ; sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; for indexing chars
    xor rbx, rbx ; for current char
.loop:
    mov bl, byte [rdi+rax]
    cmp bl, 0
    je .exit ; return if the null terminator has been encountered
    inc rax
    jmp .loop
.exit:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi ; string address
    mov rdx, rax ; string length, stored in rax after string_length call
    mov rax, 1 ; sys_write
    mov rdi, 1 ; stdout descriptor
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; push char on top of the stack
    mov rsi, rsp ; get address of the top of the stack
    mov rdx, 1 ; we are printing 1 character
    mov rax, 1 ; sys_write
    mov rdi, 1 ; stdout descriptor
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    dec rsp ; allocate 1 byte on stack
    mov byte[rsp], 0 ; write null terminator on top of the stack
    mov rsi, 1 ; number of bytes currently allocated on stack
    mov rcx, 10 ; for division
    mov rax, rdi ; number
.loop:
    xor rdx, rdx ; cleaning rdx for division
    div rcx ; divide number by 10
    add rdx, '0' ; add '0' to the remainder to get the char value of the next digit
    dec rsp ; allocate 1 byte on stack
    inc rsi ; increase counter of allocated bytes
    mov byte[rsp], dl ; move next digit to the top of the stack
    test rax, rax
    jz .exit ; quit loop if the result of the division is 0 (we've got all digits)
    jmp .loop
.exit:
    mov rdi, rsp ; address of the beginning of the string
    push rsi ; storing rsi for later use
    call print_string
    pop rsi
    add rsp, rsi ; deallocate stack memory
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .pos ; if positive, jump to print_uint
    push rdi
    mov rdi, '-' ; if negative, print '-'
    call print_char
    pop rdi
    neg rdi ; get the absolute value of the number, then jump to print_uint
.pos:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax ; for indexing chars
.loop:
    mov cl, byte[rdi + rax]
    mov dl, byte[rsi + rax]
    cmp cl, dl
    jne .ne ; quit loop if chars are not equal
    test cl, cl
    jz .eq ; quit loop if the null terminator has been reached
    inc rax
    jmp .loop ; else continue with next chars
.eq:
    mov rax, 1
    ret
.ne:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; sys_read
    xor rdi, rdi ; stdin descriptor
    push rax ; allocate memory on stack
    mov rsi, rsp ; address of the top of the stack
    mov rdx, 1 ; number of chars (1)
    syscall
    cmp rax, 0 ; return 0 if no characters have been read
    jle .err
    pop rax ; get value of character
    ret
.err:
    pop rax
    xor rax, rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
.spaces:
    ; reading chars until we encounter a non-space character or can't read anymore
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    test rax,rax
    jz .add_null_term ; if no valid characters are left in stdin, try to write null terminator to buffer
    cmp al, 0x20
    je .spaces
    cmp al, 0xA
    je .spaces
    cmp al, 0x9
    je .spaces
.read_word:    
    mov byte[rdi], al ; move first valid character to the beginning of buffer
    inc rdi ; buffer + 1 (address of the first free byte in byffer)
    dec rsi ; buffer_size - 1 (remaining space in buffer)
    push rdi
    push rsi
    xor rax, rax ; sys_read
    mov rdx, rsi ; count (remaining space in buffer)
    mov rsi, rdi ; address of the first free byte (buffer + 1)
    xor rdi, rdi ; stdin descriptor
    syscall
    pop rsi
    pop rdi
    dec rdi ; address of the beggining of the buffer
    inc rsi ; buffer size
.check_word:
    ; check for space characters in buffer, if encountered, change word length 
    inc rax ; after syscall rax contained the number of characters successfully read. adding 1 for the character that has been read with read_char
    xor rdx, rdx ; for indexing chars
    .check_loop:
        cmp rdx, rsi
	jge .fail  ; if we've reached the end of the buffer, we can't add null terminator
        cmp rdx, rax
	je .exit_check_loop ; exit if all characters have been checked
        mov bl, byte[rdi + rdx] 
        cmp bl, 0x20
	je .exit_check_loop
	cmp bl, 0xA
	je .exit_check_loop
        cmp bl, 0x9
	je .exit_check_loop ; check for spaces
	inc rdx
	jmp .check_loop
    .exit_check_loop:
        mov rax, rdx ; copying word length to rax (for .add_null_term to be able to work after jump from .spaces)
.add_null_term:    
    mov byte[rdi + rax], 0 ; adding null terminator after the word
    mov rdx, rax
    mov rax, rdi ; address of the beginning of the buffer
    jmp .exit
.fail:
    xor rax, rax
    xor rdx, rdx
.exit:
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov rcx, 10 ; for multiplication
    push rbx
    xor rbx, rbx ; for storing chars
.loop:
    mov bl, [rdi + rdx]
    test bl, bl
    jz .exit ; exit if null terminator has been reached
    cmp bl, '0'
    jl .exit
    cmp bl, '9'
    jg .exit ; checking if the character is a digit
    sub bl, '0' ; adding digit's value from char
    push rdx ; saving rdx before mul
    mul rcx
    pop rdx
    add rax, rbx ; value = (prev_value * 10) + next_digit
    inc rdx ; increasing counter
    jmp .loop
.exit:
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    ;xor rsi, rsi
    mov al, [rdi]
    test al, al
    jz .exit ; exit if the null terminator has been reached
    cmp al, '-'
    je .neg
    cmp al, '+'
    je .pos
    jmp .uns
.neg:
    inc rcx ; rcx = ( num < 0 ) ? 1 : 0
.pos:
    inc rdi ; if the first character is '+'/'-', increment address of the beginning of number to parse the absolute value of number
    mov rdx, 1 ; also increase char counter 
.uns:
    push rcx 
    push rdx ;save char counte
    call parse_uint ; parse the absolute value of the number
    pop rcx 
    add rdx, rcx ; add previous char counter to parse_uint char counter
    pop rcx
    test rcx, rcx
    je .exit ; exit if the number is positive
    neg rax ; get value of negative number
.exit:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax ; index
.loop:
    cmp rax, rdx
    je .fail ; exit if the end of the buffer has been reached before the string has been fully copied
    mov bl, [rdi + rax]
    mov [rsi + rax], bl ; copy character
    inc rax ; increment index
    cmp bl, 0
    je .exit ; exit if the null terminator has been reached
    jmp .loop
.fail:
    xor rax, rax
.exit:
    ret


print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret
