%define POINTER_SIZE 8
%include "dict.inc"
section .text

global find_word

find_word:
    .loop:
        push rdi
        push rsi
		add rsi, POINTER_SIZE ; get key address
        call string_equals ; check if the key matches
        pop rsi
        pop rdi
        test rax, rax
        jnz .success
        mov rsi, [rsi]
		test rsi, rsi 
		jnz .loop
    .fail:
        xor rax, rax
		ret
    .success:
		push rdi
		push rsi
		call string_length
		pop rsi
		pop rdi
		add rsi, POINTER_SIZE
		add rsi, rax ; adding string length
		inc rsi ; adding 1 for null terminator
		mov rax, rsi
		ret
