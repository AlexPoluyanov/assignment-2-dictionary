%ifndef HEAD_POINTER
    %define HEAD_POINTER 0
%endif

%macro colon 2
    %ifid %2
		%2: dq HEAD_POINTER
		%define HEAD_POINTER %2
	%else
		%error "Not an id"
	%endif

	%ifstr %1
		db %1, 0
	%else
		%error "Not a string"
	%endif
%endmacro
