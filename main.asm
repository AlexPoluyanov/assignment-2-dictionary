%include "words.inc"
%include "lib.inc"

section .rodata
    no_input_message: db "Input string is too long", 10, 0
    no_such_entry_message: db "No such key in the dictionary", 10, 0
    no_dictionary_message: db "The dictionary is empty", 10, 0

section .bss
    buf: resb 256

global _start

_start:
    %ifndef HEAD_POINTER
        jmp .no_dict ;  dictionary is empty
    %else
        mov rdi, buf
        mov rsi, 256
        call read_word
	    test rax, rax
        jz .no_input ; read key, fail if the key doesn't fit into the buffer
	    mov rdi, buf
        mov rsi, HEAD_POINTER
        call find_word
        test rax, rax
        jz .no_such_entry ; fail if the entry with this key isn't found
        mov rdi, rax
        call print_string
        call print_newline ; print the value from the dictionary
        mov rdi, 0
        call exit

    %endif
    .no_dict:
        mov rdi, no_dictionary_message
	    jmp .exit_with_error
    .no_input:
        mov rdi, no_input_message
	    jmp .exit_with_error
    .no_such_entry:
        mov rdi, no_such_entry_message
    .exit_with_error:
        call print_error
        mov rdi, 1
        call exit


